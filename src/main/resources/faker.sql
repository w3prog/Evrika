INSERT INTO Dealer (name, description, location, email, password, phone, IS_LEGAL_ENTITY) VALUES('ОАО рога и копыта','Большая фирма','Москва','ddd@mail.com', '1234', '1234', 'true');
INSERT INTO Dealer (name, description, location, email, password, phone, IS_LEGAL_ENTITY) VALUES('ОАО холодное мороженное','Продает мороженное','Москва','mx@mail.com', '423', '1231234', 'true');
INSERT INTO Dealer (name, description, location, email, password, phone, IS_LEGAL_ENTITY) VALUES('Иванов Иван Иваныч','Опытный предприниматель, занимается недвижемость','Иваного','mx@bk.com', '423', '891231234', 'false');
INSERT INTO Dealer (name, description, location, email, password, phone, IS_LEGAL_ENTITY) VALUES('Семенов Семен Семеныч','Молодой предприниматель, занимается извозом','КТВ','mxdf@bk.com', '42312', '890000234', 'false');
INSERT INTO Employee (name, description, location, email, password, phone, job) VALUES('Семенов Семен Семеныч','Молодой дизайнер','СПБ','mxdf1@bk.com', 'qaz', '89001234', NULL );
INSERT INTO Employee (name, description, location, email, password, phone, job) VALUES('Игоро Семен Семеныч','Молодой верстальщик','СПБ','mxdf2@bk.com', 'wsx', '89000345345', NULL );
INSERT INTO Employee (name, description, location, email, password, phone, job) VALUES('Илья Семен Семеныч','Молодой программист','СПБ','mxdf3@bk.com', 'edc', '890345', NULL );
INSERT INTO Employee (name, description, location, email, password, phone, job) VALUES('Андрей Семен Семеныч','Молодой предприниматель','СПБ','mxdf4@bk.com', 'rfv', '984534234', NULL );
INSERT INTO Employee (name, description, location, email, password, phone, job) VALUES('Егор Семен Семеныч','Молодой предприниматель','СПБ','mxdf5@bk.com', 'tgb', '573486434', NULL );
INSERT INTO Category (name) VALUES ('Создание сайтов')
INSERT INTO Category (name) VALUES ('Программирование декстопных приложений')
INSERT INTO Category (name) VALUES ('Мобильная разработка')
INSERT INTO Category (name) VALUES ('Дизайн')
INSERT INTO Category (name) VALUES ('Графика')
INSERT INTO Category (name) VALUES ('Видеоролики')
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Верстка главной страницы','Важная задача', 1,0,1);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Верстка страницы категории','Важная задача', 1,0,1);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Разработка авторизации для сайта','Важная задача', 1,0,1);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Добавление моделей в базу данных','Важная задача', 1,0,1);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Добавление моделей в базу данных','Важная задача', 1,0,1);

INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Разработка приложения для медицины','Важная задача', 2,0,2);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Разработка приложения для ресторана','Важная задача', 1,0,2);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Разработка приложения для магазина','Важная задача', 1,0,2);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Создание приложения IOS','Важная задача', 1,0,3);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Создание приложения Android','Важная задача', 2,0,3);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Создание приложения WP','Важная задача', 3,0,3);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Создание диайна магазина','Важная задача', 1,0,4);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Разработка логотипа','Важная задача', 2,0,4);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Редизайн лендинга','Важная задача', 3,0,4);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Создание иллюстраций','Важная задача', 1,0,5);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Рисование нового логотипа','Важная задача', 2,0,5);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Расширение цветовой гаммы проекта','Важная задача', 3,0,5);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Сделать свадебный ролик','Важная задача', 1,0,6);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Разработка рекламного ролика','Важная задача', 2,0,6);
INSERT INTO Job (name, description, employee, status, categoryId) VALUES('Смонтировать видео спортивного мероприятия','Важная задача', 3,0,6);