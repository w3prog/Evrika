package io.evrika.config;

import org.springframework.security.core.GrantedAuthority;

public final class Authorities {

    private Authorities() {
    };

    public static final GrantedAuthority DEALER = () -> "DEALER";

    public static final GrantedAuthority EMPLOYEE = () -> "EMPLOYEE";

}
