package io.evrika.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Employee extends BaseUser {

    @OneToOne(fetch = FetchType.LAZY)
    private Job job;
//    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    private List<Skill> skills;

    @ManyToOne
    @JoinColumn(name = "categoryId")
    private Category category;

    public Employee() {
    }

    public Job getJob() {
        return job;
    }
    public boolean isFreeEmployee(){
        return job==null;
    }

    public void setJob(Job job) {
        this.job = job;
    }

//    public List<Skill> getSkills() {
//        return skills;
//    }
//
//    public void setSkills(List<Skill> skills) {
//        this.skills = skills;
//    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
