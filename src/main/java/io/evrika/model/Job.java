package io.evrika.model;

import javax.persistence.*;

@Entity
public class Job extends BaseEntity {

    private String name;
    private String description;
    private String location;

    @ManyToOne
    @JoinColumn(name = "dealerId")
    private Dealer dealer;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "job")
    private Employee employee;
    private short status;

    @ManyToOne
    @JoinColumn(name = "categoryId")
    private Category category;


    public Job() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
