package io.evrika.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;


@Entity
public class Dealer extends BaseUser {

    @NotNull
    @JoinColumn(name = "legalEntity")
    private boolean isLegalEntity;

    @OneToMany(mappedBy = "dealer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Job> jobList;

    public Dealer() {
    }

    public boolean isLegalEntity() {
        return isLegalEntity;
    }

    public void setLegalEntity(boolean legalEntity) {
        isLegalEntity = legalEntity;
    }
}
