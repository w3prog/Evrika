package io.evrika.repository;

import io.evrika.model.Proc;
import org.springframework.data.repository.CrudRepository;
//не стал разбираться в тонкостях настройки.

public interface ProcRepository {

    Proc findByEmail(int employeeId);

}