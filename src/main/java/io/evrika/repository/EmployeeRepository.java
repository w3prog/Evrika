package io.evrika.repository;


import io.evrika.model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    Employee findByEmail(String email);
}
