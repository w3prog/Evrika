package io.evrika.repository;

import io.evrika.model.Category;
import org.springframework.data.repository.CrudRepository;


public interface CategoryRepository extends CrudRepository<Category, Long> {
    Category findByName(String name);
}
