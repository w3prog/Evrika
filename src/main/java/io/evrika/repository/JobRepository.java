package io.evrika.repository;


import io.evrika.model.Category;
import io.evrika.model.Job;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JobRepository extends CrudRepository<Job, Long> {
    List<Job> findByCategory(Category category);
}
