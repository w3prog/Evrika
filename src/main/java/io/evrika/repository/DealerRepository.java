package io.evrika.repository;


import io.evrika.model.Dealer;
import org.springframework.data.repository.CrudRepository;

public interface DealerRepository extends CrudRepository <Dealer, Long> {

    Dealer findByEmail(String email);

}
