package io.evrika.service;

import io.evrika.config.Authorities;
import io.evrika.model.Dealer;
import io.evrika.model.Employee;
import io.evrika.repository.DealerRepository;
import io.evrika.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;

@Service
public class BaseUserService implements UserDetailsService {

    @Autowired
    private DealerRepository dealerRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//        Employee dealer = new Employee();
//        dealer.setEmail("x@x.com");
//        dealer.setPassword("xxx");
        Dealer dealer = dealerRepository.findByEmail(email);
        GrantedAuthority grantedAuthority = Authorities.DEALER;
        Employee employee = employeeRepository.findByEmail(email);
        if (dealer == null) {
            grantedAuthority = Authorities.EMPLOYEE;
            if (dealer == null && employee == null) {
                return null;
            }


        }

        GrantedAuthority finalGrantedAuthority = grantedAuthority;
        return new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return Collections.singletonList(finalGrantedAuthority);
            }

            @Override
            public String getPassword() {
                if (finalGrantedAuthority.equals(Authorities.DEALER)) {
                    return dealer.getPassword();
                }else  return employee.getPassword();
            }

            @Override
            public String getUsername() {
                if (finalGrantedAuthority.equals(Authorities.DEALER)) {
                    return dealer.getEmail();
                }else  return employee.getEmail();
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return true;
            }
        };
    }
}
