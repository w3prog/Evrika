package io.evrika.controller;

import io.evrika.model.Dealer;
import io.evrika.model.Job;
import io.evrika.repository.CategoryRepository;
import io.evrika.repository.DealerRepository;
import io.evrika.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DealerController {

    @Autowired
    private DealerRepository dealerRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private JobRepository jobRepository;

    @RequestMapping("/deals")
    String deals(Model model) {
        return "dealer_pages/deals";
    }

    @RequestMapping(value = "/add_deal", method = RequestMethod.GET)
    String addDeal(Model model) {
        return "dealer_pages/add_deal";
    }

    @RequestMapping(value = "/add_deal", method = RequestMethod.POST)
    String addDealPost(Model model,
                       @RequestParam(value = "region", required = false) String region,
                       @RequestParam(value = "name", required = false) String name,
                       @RequestParam(value = "desc", required = false) String desc,
                       @RequestParam(value = "category", required = false) String category,
                       @RequestParam(value = "type_target", required = false) String type_target,
                       Authentication authentication) {
        try {
            Job job = new Job();
            if (authentication != null) {
                job.setDealer(dealerRepository.findByEmail(((UserDetails) authentication.getPrincipal()).getUsername()));
            }
            job.setName(name);
            job.setDescription(desc);
            job.setLocation(region);
            job.setCategory(categoryRepository.findByName(category));

            jobRepository.save(job);
        } catch (Exception e) {
            return "dealer_pages/add_deal_data_control";
        }
        return "dealer_pages/dashboard";

    }

    @RequestMapping(value = "/register_new_dealer", method = RequestMethod.POST)
    String registerNewDealer(Model model, @RequestParam(value = "email", required = false) String email,
                             @RequestParam(value = "inputPassword1", required = false) String password1,
                             @RequestParam(value = "inputPassword2", required = false) String password2,
                             @RequestParam(value = "phone", required = false) String phone,
                             @RequestParam(value = "region", required = false) String region,
                             @RequestParam(value = "desc", required = false) String desc,
                             @RequestParam(value = "type", required = false) String type,
                             @RequestParam(value = "name", required = false) String name) {

        Dealer dealer = new Dealer();
        try {
            dealer.setEmail(email);
            if (password1 == password2) {
                dealer.setPassword(password1);
            }
            dealer.setPhone(phone);
            dealer.setLocation(region);
            dealer.setDescription(desc);
            if (type.equalsIgnoreCase("legal")) {
                dealer.setLegalEntity(true);
            }
            dealer.setName(name);
            dealerRepository.save(dealer);
        } catch (Exception e) {
            return "dealer_pages/register";
        }
        return "dealer_pages/dashboard";
    }

    @RequestMapping(value = "employee_add_to_project", method = RequestMethod.POST)
    String employeeAddToProject(Model model) {
        //// TODO: 11/19/16 реализовать возможность предтвердить пользователя как исполнителя.
        return "dealer_pages/dashboard";
    }

    @RequestMapping(value = "/select_employee", method = RequestMethod.GET)
    String selectEmployeed(Model model) {
        //// TODO: 11/19/16 реализовать возможность поисмотреть исполнителя
        return "dealer_pages/select_employee";
    }


    @RequestMapping(value = "/select_employee/{id}", method = RequestMethod.POST)
    String selectEmployee(Model model) {
        //// TODO: 11/19/16 реализовать возможность поисмотреть исполнителя
        return "dealer_pages/dashboard";
    }
}