package io.evrika.controller;

import io.evrika.config.Authorities;
import io.evrika.model.Job;
import io.evrika.repository.JobRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import io.evrika.model.Dealer;
import io.evrika.repository.DealerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.security.Principal;

@Controller
public class HomeController {

    @Autowired
    private DealerRepository dealerRepository;

    @Autowired
    private JobRepository jobRepository;

    @RequestMapping("/")
    public String login(Model model, Authentication authentication) {
        if(authentication != null) {
            System.out.println(authentication);
        }
        boolean isFreeEmployee = false;
        if (authentication==null)
            //return "all_pages/login";
            return "all_pages/index";
        else if (authentication.getAuthorities().contains(Authorities.DEALER)){
            return "dealer_pages/dashboard";
        } else if (isFreeEmployee)
                return "employee_pages/dashboardfree";
            else
                return "employee_pages/dashboard";
    }


    @RequestMapping(value = "/register",method = RequestMethod.GET)
    public String registerPost(Model model) {
            return "all_pages/register";
    }

    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public String registerPost(Model model,
                               @RequestParam(value = "type", required = false) String type) {
    if(type.equals("Dealer"))
            return "dealer_pages/register";
        else
                return "employee_pages/register";
    }
    /*
    Карта маршрутизации
     */
    @RequestMapping("/udpate_profile")
    public String udpateProfile(Model model) {
        //Todo Проверка типа пользовать.
        boolean isDealer = true;
        if (isDealer){
            return "dealer_pages/dealer_profile";
        }else {
            return "employee_pages/employee_profile";
        }
    }

    @RequestMapping("/loqout")
    public String logout(Model model){
        SecurityContextHolder.getContext().setAuthentication(null);
        return "all_pages/login";
    }

    /*
    Разберить с правильность записи id в пути
     */
    @RequestMapping("/deals/{id}")
    public String dealsId(ModelMap model, @PathVariable("id") Long id){
        Job job = jobRepository.findOne(id);
        model.addAttribute("deal", job);
        return "dealer_pages/deal";
    }

    @RequestMapping("/foggot_password")
    public String foggotPassword(Model model){
        //// TODO: 11/19/16 Заполнение данных из базы данных в шаблон.
        return "all_pages/forgotpassword";
    }


}