package io.evrika.controller;

import io.evrika.model.Category;
import io.evrika.model.Employee;
import io.evrika.repository.CategoryRepository;
import io.evrika.repository.EmployeeRepository;
import io.evrika.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping("/deal_search")
    String dealSearch(ModelMap model, Authentication authentication) {
        //Вывести список заказов по кваливикации разработчика
        Employee employee = employeeRepository.findByEmail(((UserDetails) authentication.getPrincipal()).getUsername());
        Category category = employee.getCategory();
        model.addAttribute("jobs", jobRepository.findByCategory(category));
        return "employee_pages/search";
    }

    /*
    Todo не реализовывать
     */
    @RequestMapping("/deal_search/{1}")
    String dealSearchPage(Model model) {
        //Вывести список заказов по кваливикации разработчика
        return "employee_pages/search";
    }

    @RequestMapping(value = "/port", method = RequestMethod.GET)
    String port(Model model) {
        return "employee_pages/port";
    }

    @RequestMapping(value = "/register_new_employee", method = RequestMethod.POST)
    String registerNewEmployee(Model model,
                               @RequestParam(value = "email", required = false) String email,
                               @RequestParam(value = "inputPassword1", required = false) String password1,
                               @RequestParam(value = "inputPassword2", required = false) String password2,
                               @RequestParam(value = "phone", required = false) String phone,
                               @RequestParam(value = "region", required = false) String region,
                               @RequestParam(value = "desc", required = false) String desc,
                               @RequestParam(value = "name", required = false) String name,
                               @RequestParam(value = "cateogry_skills", required = false) String category) {
        Employee employee = new Employee();
        try {
            employee.setEmail(email);
            if (password1 == password2) {
                employee.setPassword(password1);
            }
            employee.setPhone(phone);
            employee.setLocation(region);
            employee.setDescription(desc);
            employee.setCategory(categoryRepository.findByName(category));

            employee.setName(name);
            employeeRepository.save(employee);
        } catch (Exception e) {
            return "employee_pages/register";
        }
        return "employee_pages/dashboardfree";
    }
}